<?php
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP1020\Hobby\Hobbies;
$obj= new Hobbies();
$single_data=$obj->prepare($_GET)->edit();
//var_dump($single_data);
?>

<!DOCTYPE html>
<html>
<body>

<form action="update.php" method="post">
    <input type="hidden"   name="id" value="<?php echo $single_data['id']?>">

    <input type="checkbox" name="hobby[]" value="Cricket"
        <?php if (preg_match("/Cricket/", $single_data['hobby'])) { echo "checked";} else {echo "";} ?> />Cricket
    <input type="checkbox" name="hobby[]" value="Football"
        <?php if (preg_match("/Football/", $single_data['hobby'])) { echo "checked";} else {echo "";} ?> />Football
    <input type="checkbox" name="hobby[]" value="Coding"
        <?php if (preg_match("/Coding/", $single_data['hobby'])) { echo "checked";} else {echo "";} ?> />Coding
    <input type="checkbox" name="hobby[]" value="Gardening"
        <?php if (preg_match("/Gardening/", $single_data['hobby'])) { echo "checked";} else {echo "";} ?> />Gardening
    <input type="submit" value="Update">
</form>

</body>
</html>
