<?php
include_once('../../../vendor/autoload.php');

use App\BITM\SEIP1020\ProfilePicture\ImageUploader;
$obj= new ImageUploader();
$single_info=$obj->prepare1($_GET)->edit();
?>

<html>
<head>
    <title>Profile Information</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link class="jsbin" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.0/jquery-ui.min.js"></script>
    <meta charset=utf-8 />
</head>
<body>
<H1>Profile Information</H1>

<form action="update.php" method="post" enctype="multipart/form-data">
    <fieldset>
        <legend>Add Profile Information</legend>
        <input type="hidden"
               name="id"
               placeholder="Please enter your name"
               value="<?php echo $single_info['id']?>"
            />

        <div>
            <label for="title">Enter your name</label>
            <input type="text"
                   name="person_name"
                   placeholder="Please enter your name"
                   value="<?php echo $single_info['profile_name']?>"
                />
        </div>
        <div>
            <label>Enter your picture</label>
            <input
                name="photo"
                placeholder="Please enter author name"
                type="file"
                value="<?php echo $single_info['image']?>"
                onchange="readURL(this);"
                />
            <img id="blah" src="<?php echo "../../../image/".$single_info['image']?>" alt="No images" height="200 px" width="200 px"
                 alt="your image" />

        </div>
        <button  type="submit" tabindex="0">Update</button>

    </fieldset>
</form>

<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
</body>
</html>
