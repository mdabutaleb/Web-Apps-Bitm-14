<?php
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP1020\ProfilePicture\Message;
use \App\BITM\SEIP1020\ProfilePicture\ImageUploader;

$obj= new ImageUploader();
$all_info=$obj->prepare1()->index();
?>

<a href="create.php">Add Again</a>
<!DOCTYPE html>
<html>
<body>
<title>List of Books</title>
<div class="warning">
  <?php
  if(array_key_exists('message',$_SESSION)&& !empty($_SESSION['message'])){
    echo Message::message();
  }
  ?>
 

<table border="1" style="width:100%">
  <tr>
    <td>SL</td>
    <td>ID</td>
    <td>Full Name</td>
    <td>Profile Picture</td>
      <td>Actions</td>

  </tr>
    <?php
    $serial=0;
    foreach($all_info as $info){
        $serial++;

    ?>
  <tr>
    <td><?php echo $serial; ?></td>
    <td> <?php echo $info['id'] ?></td>
    <td><?php echo $info['profile_name']?></td>
    <td><img src="<?php echo "../../../image/".$info['image']?>" alt="No images" height="200 px" width="200 px"></td>
      <td><a href="show.php?id=<?php echo $info['id'] ?>">View</a>|
          <a href="edit.php?id=<?php echo $info['id'] ?>">Edit</a>|
          <a href="delete.php?id=<?php echo $info['id'] ?>">delete</a></td>
  </tr>
    <?php }?>


</table>
  

</body>
</html>





