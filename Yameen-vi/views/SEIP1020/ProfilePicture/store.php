<?php
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP1020\ProfilePicture\ImageUploader;

//var_dump($_POST);

if(isset($_FILES['photo'])){
    $errors= array();
    $file_name = time().$_FILES['photo']['name'];
    $file_size =$_FILES['photo']['size'];
    $file_tmp =$_FILES['photo']['tmp_name'];
    $file_type=$_FILES['photo']['type'];
    $file_ext=strtolower(end(explode('.',$_FILES['photo']['name'])));

    $formats= array("jpeg","jpg","png");

    if(in_array($file_ext,$formats)=== false){
        $errors[]="extension not allowed, please choose a JPEG or PNG file.";
    }

    if($file_size > 2097152){
        $errors[]='File size must be excately 2 MB';
    }

    if(empty($errors)==true){
        move_uploaded_file($file_tmp,"../../../Image/".$file_name);
        $_POST['image']=$file_name;
    }else{
        print_r($errors);
    }
}

$profile= new ImageUploader();
$profile->prepare1($_POST)->store();

?>