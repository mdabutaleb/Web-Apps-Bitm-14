<!DOCTYPE html>
<html>
<head>
    <title>Profile Information</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<H1>Profile Information</H1>

<form action="store.php" method="post" enctype="multipart/form-data">
    <fieldset>
        <legend>Add Profile Information</legend>

        <div>
            <label for="title">Enter your name</label>
            <input type="text"
                name="person_name"
                placeholder="Please enter your name"
                />
        </div>
        <div>
            <label>Enter your picture</label>
            <input
                name="photo"
                placeholder="Please enter author name"
                type="file"
                />
        </div>
        <div>
        <button  type="submit" tabindex="0">Save</button>
        </div>

    </fieldset>
</form>
</body>
</html>
