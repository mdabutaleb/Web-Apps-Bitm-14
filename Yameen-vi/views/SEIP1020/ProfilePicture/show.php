<?php
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP1020\ProfilePicture\ImageUploader;


$obj= new \App\BITM\SEIP1020\ProfilePicture\ImageUploader();
$singleData= $obj->prepare1($_GET)->show();

?>


<!DOCTYPE html>
<html>
<body>
<a href="index.php">Back to index</a>
<h2>ID</h2>

<ul style="list-style-type:circle">
    <li><?php echo $singleData['id'] ?></li>
</ul>

<h2>Profile Name</h2>

<ul style="list-style-type:circle">
    <li><?php echo $singleData['profile_name'] ?></li>
</ul>

<h2>Profile Picture</h2>

<ul style="list-style-type:circle">
    <li><img src="<?php echo "../../../image/".$singleData['image'] ?>" alt="Smiley face" height="100" width="100"></li>
</ul>
</body>
</html>
