<!DOCTYPE html>
<html>
<head>
    <title>Create an Item</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<H1>Create an Item</H1>

<form action="store.php" method="post">
    <fieldset>
        <legend>Add Book Title</legend>

        <div>
            <label for="title">Enter Book Title</label>
            <input
                name="title"
                id="title"
                autofocus="true"
                placeholder="Please enter your book name"
                />
        </div>


        <button  type="submit">Save</button>
        <button  type="submit">Save & Create Again</button>
        <input type="reset" value="Reset" />
    </fieldset>
</form>
</body>
</html>
