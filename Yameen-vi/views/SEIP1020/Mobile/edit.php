<?php
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP1020\Mobile\Mobile;

$mobile = new Mobile();
$theMobile = $mobile->prepare($_GET)->edit();

?>

<html>
<head>
    <title>Edit an Item</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<H1>Edit an Item</H1>

<form action="update.php" method="post">
    <fieldset>
        <legend>Edit Book </legend>
        <input type="hidden" name="id" value="<?php echo $theMobile->id;?>" />
        <div>
            <label for="title"> Mobile Title</label>
            <input
                name="title"
                id="title"
                autofocus="true"
                tabindex="10"
                placeholder="Please enter your book name"
                value="<?php echo $theMobile->title;?>"
                />
        </div>



        <button  type="submit" tabindex="0">Save</button>
        <input type="reset" value="Reset" />
    </fieldset>
</form>
<nav>
    <li><a href="index.php">Go to List</a></li>
</nav>
</body>
</html>
