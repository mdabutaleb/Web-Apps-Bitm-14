<?php
include_once('../../../vendor/autoload.php');

use \App\BITM\SEIP1020\Mobile\Mobile;
use  \App\BITM\SEIP1020\Mobile\Utility;

use \App\BITM\SEIP1020\Mobile\Message;

$mobiles = new Mobile();
$mobile = $mobiles->trashed();



?>

<!DOCTYPE html>
<html>
<head>
    <title>Trashed</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        #message{
            background-color:green;
        }

    </style>
</head>
<body>
<h1>Mobile Title</h1>

<div id="message">
    <?php echo Message::message(); ?>
</div>
<table border="1">
        <thead>
        <tr>
            <th>Sl.</th>
            <th>ID</th>
            <th>Title&dArr;</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if(count($mobile) > 0){

            $slno =1;
            foreach($mobile as $_mobile){
                ?>
                <tr>
                    <td><?php echo $slno;?></td>
                    <td><?php echo $_mobile['id'];?></td>
                    <td><a href="show.php?id=<?php echo $_mobile['id'];?>"><?php echo $_mobile['title'];?></a></td>
                    <td>
                        <a href="recover.php?id=<?php echo $_mobile['id'];?>">Recover</a>
                        | <a href="delete.php?id=<?php echo $_mobile['id'];?>" class="delete">Delete</a>

                    </td>
                </tr>
                <?php
                $slno++;
            }

        }else{
            ?>
            <tr>
                <td colspan="6">No record is available.</td>
            </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
</form>

<div><span> prev  1 | 2 | 3 next </span></div>

<nav>
    <li><a href="index.php">Go to list</a></li>
</nav>
<script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript" ></script>
<script>


    $(document).ready(function() {

        $('.delete').bind('click', function (e) {
            var deleteItem = confirm("Are you sure you want to delete?");
            if (!deleteItem) {
                //return false;
                e.preventDefault();
            }
        });
        $('#message').hide(5000);


    }




</script>
</body>
</html>
