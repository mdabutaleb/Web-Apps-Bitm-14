<?php
include_once("../../../vendor/autoload.php");
session_start();

use App\BITM\SEIP1020\Mobile\Mobile;
use App\BITM\SEIP1020\Mobile\Message;


$mobile = new Mobile();


if(array_key_exists('itemPerPage',$_SESSION)){
    if(array_key_exists('itemPerPage',$_GET)){
        $_SESSION['itemPerPage']=$_GET['itemPerPage'];
    }
}else{
    $_SESSION['itemPerPage']=10;

}
if(array_key_exists('pageNumber',$_GET)){
    $pagenumber=$_GET['pageNumber'];

}
else{
    $pagenumber=1;
}



$itemPerPage=$_SESSION['itemPerPage'];

$pageStartFrom=$itemPerPage*($pagenumber-1);


$itemPerPage=$_SESSION['itemPerPage'];
$totalItems= $mobile->count();

$totalPage=ceil($totalItems/$itemPerPage);
$paging="";
for($i=1;$i<=$totalPage;$i++){

    $paging .= '<a href="index.php?pageNumber='.$i.'">'.$i.'</a>'.' | ';
}

$mobiles=$mobile->paginate($pageStartFrom,$itemPerPage);

?>


<!DOCTYPE html>
<html>
<head>
    <style>
        .warning{
            background-color: green;
        }


    </style>
    <title>List of Books</title>
    <div class="warning">
        <?php
        if(array_key_exists('message',$_SESSION)&& !empty($_SESSION['message'])){
            echo Message::message();
        }

        ?>

    </div>
  

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../Resources/alertify/dist/alertify.css" />
    <link rel="stylesheet" href="../../../Resources/alertify/dist/themes/alertify.default.css" />


    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>


</head>

<body>
<h1>List Of Mobiles</h1>
<div>
    <?php echo substr($paging,0,strlen($paging)-2); ?>
</div>
<a href="trashed.php">All Trashed Data</a>


<div><span>Search / Filter </span>
    <span id="utility"> <a href="pdf.php" target="_blank">Download as PDF</a> | <a href="xl.php">XL</a>| <a href="phpmailer.php">Email to friend</a> |<a href="create.php">Create New</a></span>
    <form action="index.php">
        <select name="itemPerPage">
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="30">30</option>
            <option value="40">40</option>
            <option value="50">50</option>
        </select>
        <button type="submit"> GO </button>
    </form>
</div>
<table border="1">
    <thead>
    <tr>
        <th>Sl.</th>
        <th>Book Title &dArr;</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $slno =0;
    foreach($mobiles as $allmobile){
        $slno++;
        ?>
        <tr>
            <td><?php echo $slno+$pageStartFrom;?></td>

            <td><a href="show.php?id=<?php echo $allmobile['id'];?>"><?php echo $allmobile['title'];?></a></td>
            <td> <a href="edit.php?id=<?php echo $allmobile['id'];?>">Edit</a>
                | <a href="delete.php?id=<?php echo $allmobile['id'];?>">Delete</a>
                <form action="delete.php" method="post">
                    <input type="hidden" name="id" value="<?php echo $allmobile['id'];?>">
                    <button class="delete" type="submit">Delete2</button>
                </form>
                | <a href="trash.php?id=<?php echo $allmobile['id']?>">Trash</a>
                | Email to Friend </td>
        </tr>
    <?php
    }
    ?>
    </tbody>
</table>
<script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript" ></script>
<script>
    $(document).ready(function() {
        $(".warning").show().delay(500).fadeOut('slow');
    });
</script>
<script>
    alertify.confirm("delete", function (e) {
        if (e) {
            // user clicked "ok"
        } else {
            // user clicked "cancel"
        }
    });
</script>



</body>
</html>
