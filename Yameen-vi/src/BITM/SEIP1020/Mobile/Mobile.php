<?php

namespace App\BITM\SEIP1020\Mobile;
use PDO;
class Mobile
{

    public $id = "";
    public $title = "";
    public $user = "root";
    public $pass = "";
    public $conn;


    public function __construct()
    {
        $this->conn = new PDO('mysql:host=localhost;dbname=atomicprojectp4', $this->user, $this->pass);
        $this->conn->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );

    }

    public function prepare($data = array())
    {
        if (is_array($data) && array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('id', $data) && !empty($data['id'])) {
            $this->id = $data['id'];
        }

        return $this;
    }

    public function store()
    {
        $query = "INSERT INTO `atomicprojectp4`.`mobiles` (`id`, `title`)
                  VALUES (NULL, :title)";
        $result = $this->conn->prepare($query);
        $result->execute(array(':title' => $this->title));

        if ($result) {
            Message::message('
             data has been added successfully.');
        } else {
            Message::message('There is an error while storing mobile information, please try again.');
        }

        header('Location:index.php');
    }

    public function index()
    {
        $allMobiles = array();
        $query = "SELECT * FROM `atomicprojectp4`.`mobiles` WHERE deleted_at IS NULL";
        $result = $this->conn->query($query) or die("failed!");
        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $allMobiles[] = $row;
        }
        return $allMobiles;

    }

    public function show()
    {
        $query = "SELECT * FROM `atomicprojectp4`.`mobiles` WHERE id = :id";
        $result = $this->conn->prepare($query);
        $result->execute(array(':id' => $this->id));
        $_mobile = $result->fetch(PDO::FETCH_ASSOC);
        return $_mobile;

    }

    public function edit()
    {

        $query = "SELECT * FROM `mobiles` WHERE id = :id";
        $result = $this->conn->prepare($query);
        $result->execute(array(':id' => $this->id));
        $mobiles = $result->fetch(PDO::FETCH_ASSOC);

        return $mobiles;

    }

    public function update()
    {
        $query = "UPDATE `atomicprojectp4`.`mobiles` SET `title` =:title WHERE id =:id";
        $result = $this->conn->prepare($query);
        $result->execute(array('title' => $this->title, ':id' => $this->id));
        if (mysql_query($result)) {
            Message::message("Book data has been  updated successfully.");
        } else {
            Message::message("There is an error while storing book information, please try again.");
        }

    }

    public function delete()
    {

        $query = "DELETE FROM `atomicprojectp4`.`mobiles` WHERE id=:id";
        $result = $this->conn->prepare($query);
        $result->execute(array('id' => $this->id));
        if ($result) {
            Message::message('Mobile data has been deleted successfully.');
        } else {
            Message::message('There is an error while deleting mobile information, please try again.');
        }
        header('Location:index.php');
    }

    public function trash()
    {

        $this->deleted_at = time();
        $query = "UPDATE `atomicprojectp4`.`mobiles` SET `deleted_at` = :deleted_at
                  WHERE `mobiles`.`id` = :id";

        $result = $this->conn->prepare($query);
        $result->execute(array(':deleted_at' => $this->deleted_at, ':id' => $this->id));

        if ($result) {
            Message::message("Mobile information has been trashed successfully.");
        } else {
            Message::message(" Cannot trash.");
        }

        Utility::redirect();
    }

    public function trashed()
    {

        $_mobiles = array();


        $query = "SELECT * FROM `mobiles` WHERE deleted_at IS NOT NULL";

        //$result = mysql_query($query);

        $result = $this->prepare($query);

        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $_mobiles[] = $row;
        }
        return $_mobiles;
    }

    public function recover()
    {
        $query = "UPDATE `atomicprojectp4`.`mobiles` SET `deleted_at` = NULL WHERE `mobiles`.`id` = :id";
        //$result = mysql_query($query);
        $result = $this->prepare($query);
        $result->execute(array(':id' => $this->id));

        if ($result) {
            Message::message("Mobile data has been  recovered successfully.");
        } else {
            Message::message(" Cannot recover.");
        }

        Utility::redirect('index.php');
    }

    public function count()
    {
        $query = "SELECT COUNT(*) AS TotalItems FROM `mobiles`;";
        $result = $this->conn->query($query);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        //$row= mysql_fetch_assoc($result);
        return $row['TotalItems'];
    }

    public function paginate($pageStartFrom = 0, $Limit = 10)
    {
        $allMobiles = array();
        $query = "SELECT * FROM `atomicprojectp4`.`mobiles` LIMIT :pageStartFrom,:Limit ";
        $result = $this->conn->prepare($query);
        $result->bindValue(':pageStartFrom',$pageStartFrom, PDO::PARAM_INT);
        $result->bindValue(':Limit',$Limit, PDO::PARAM_INT);
        $result->execute();
        $allMobiles = $result->fetchAll(PDO::FETCH_ASSOC);


       return $allMobiles;
    }
}