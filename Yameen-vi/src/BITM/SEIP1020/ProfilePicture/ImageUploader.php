<?php
namespace App\BITM\SEIP1020\ProfilePicture;
session_start();

use App\BITM\SEIP1020\ProfilePicture\Message;
use App\BITM\SEIP1020\ProfilePicture\Utility;
use PDO;

class ImageUploader
{
    public $id = "";
    public $image = "";
    public $name = "";
    public $username = "root";
    public $password = "";
    public $conn = "";


    public function __construct()
    {
        try {
            $this->conn = new PDO('mysql:host=localhost;dbname=atomicprojectp4', $this->username, $this->password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }

    }


    public function prepare1($data = array())
    {
        if (is_array($data) && array_key_exists('person_name', $data)) {
            $this->name = $data['person_name'];

        }
        if (is_array($data) && array_key_exists('image', $data)) {
            $this->image = $data['image'];

        }

        if (array_key_exists('id', $data) && !empty($data['id'])) {
            $this->id = $data['id'];

        }

        return $this;

    }


    public function store()

    {
        if (!empty($this->name) || !empty($this->image)) {
            $query = "INSERT INTO `profilepic` (`profile_name`,`image`) VALUES (:profilename,:image )";
            $result = $this->conn->prepare($query);
            $result->execute(array(':profilename'=>$this->name,':image'=>$this->image));
        }
        if ($result) {
            Message::message("Profile data has been inserted successfully");
            Utility::redirect();
        } else {
            Message::message("Data error");
            header('Location:create.php');
        }


    }


    public function index()
    {
        $data= array();
        $query="SELECT * FROM `atomicprojectp4`.`profilepic";
        $q = $this->conn->query($query) or die("failed!");
        $data = $q->fetchAll(PDO::FETCH_ASSOC);

        return $data;

    }


    public function show(){
        $query="SELECT * FROM `atomicprojectp4`.`profilepic` WHERE id = :id";
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->id));
        $singleData = $result->fetch(PDO::FETCH_ASSOC);
            return $singleData;

    }

    public function edit()
    {
        $query="SELECT * FROM `atomicprojectp4`.`profilepic` WHERE id = :id";
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->id));
        $singleData = $result->fetch(PDO::FETCH_ASSOC);
            return $singleData;

    }


    public function update()
    {
        if (!empty($this->image)) {
            $query = "UPDATE `profilepic` SET `profile_name` = :name, `image` = :image WHERE `profilepic`.`id` = :id";
            $result = $this->conn->prepare($query);
            $result->execute(array(':name' => $this->name, ':image' => $this->image, ':id' => $this->id));
        }else {
            $query = "UPDATE `atomicprojectp4`.`profilepic` SET `profile_name` = :name  WHERE `profilepic`.`id` = :id";
            $result = $this->conn->prepare($query);
            $result->execute(array(':name' => $this->name,':id'=>$this->id));
        }
       
        if ($result) {
            Message::message("Profile data has been updated successfully");
            Utility::redirect();

        } else {
            Message::message("Error");
        }
    }


    public function delete()
    {

        $query = "DELETE FROM `profilepic` WHERE `profilepic`.`id` =:id";
        $result = $this->conn->prepare($query);
        $result->execute(array(':id'=>$this->id));

        if ($result) {
            Message::message("Profile data has been deleted successfully");
            Utility::redirect();

        } else {
            Message::message("Error");
        }
    }


}