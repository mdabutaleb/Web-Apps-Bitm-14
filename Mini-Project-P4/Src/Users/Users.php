<?php
namespace App\Users;

use App\Utility\Utility;
use PDO;

class Users
{
    public $dbuser = 'root';
    public $dbpassword = '';
    public $conn = '';
    public $id = '';
    public $title = '';
    public $username = '';
    public $password = '';
    public $email = '';
    public $lastinsertid = '';


    public function __construct()
    {
        try {
            $this->conn = new PDO('mysql:host=localhost;dbname=usrreg', $this->dbuser, $this->dbpassword);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function prepare($data = '')
    {
//        Utility::dd($data);
        if (array_key_exists('username', $data) && !empty($data['username'])) {
            $this->username = $data['username'];
        }
        if (array_key_exists('password', $data) && !empty($data['password'])) {
            $this->password = $data['password'];
        }
        if (array_key_exists('email', $data) && !empty($data['email'])) {
            $this->password = $data['email'];
        }
        return $this;

    }

    public function signup()
    {
        try {
            $stmt = $this->conn->prepare('INSERT INTO `users` (`username`, `password`, `email`)
                                                      VALUES(:un, :pw, :email)');
            $stmt->execute(array(
                ':un' => $this->username,
                ':pw' => $this->password,
                ':email' => $this->email
            ));

            $stmt2 = $this->conn->prepare('INSERT INTO `profiles` (`user_id`, `created_at`)
                                                      VALUES(:uid, :created_at)');
            $stmt2->execute(array(
                ':uid' => $this->conn->lastInsertId(),
                ':created_at' => date("Y-m-d h:i:sa"),
            ));

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        header('location:../../login.php');

    }

    public function login(){

    }
}