<?php

namespace App\Users;
use PDO;

class Users
{
    public $id='';
    public $username='';
    public $password='';
    public $email='';
    public $data = '';
    public $conn='';
    public $dbuser='root';
    public $dbpass='';

    public function __construct()
    {
        session_start();
        try {
            $this->conn = new PDO('mysql:host=localhost;dbname=owncms', $this->dbuser, $this->dbpass);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            echo "Connected";
        } catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

}