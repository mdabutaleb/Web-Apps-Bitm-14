<?php
/**
 * Created by PhpStorm.
 * User: MD
 * Date: 4/4/2016
 * Time: 3:19 PM
 */

namespace App\BITM\SEIP50\Imageuploader;

use App\BITM\SEIP50\Utility\Utility;
use PDO;


class Imageuploader
{
    public $id = '';
    public $title = '';
    public $username = 'root';
    public $password = '';
    public $profile_name = '';
    public $image_name = '';
    public $conn = '';

    public function  __construct()
    {
        try {
            $this->conn = new PDO("mysql:host=localhost;dbname=atomicprojectp4", $this->username, $this->password) or die('Unabel to Connect');
            // set the PDO error mode to exception
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//            echo "Connected successfully";
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public function prepare($data = '')
    {
//        Utility::debug($data);
        if (array_key_exists('name', $data) && !empty($data['name'])) {
            $this->profile_name = $data['name'];
        }
        if (array_key_exists('img', $data) && !empty($data['img'])) {
            $this->image_name = $data['img'];
        }
        if (array_key_exists('id', $data) && !empty($data['id'])) {
            $this->id = $data['id'];
        }

        return $this;

    }

    public function store()
    {
        try {
            $stmt = "INSERT INTO `profilepic`(`profile_name`, `image`) VALUE (:pn, :im)";
            $q = $this->conn->prepare($stmt);
            $q->execute(array(
                ':pn' => $this->profile_name,
                ':im' => $this->image_name
            ));
            if ($q) {
                session_start();
                $_SESSION['message'] = "Uploaded Successfully";
                header('location:create.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function index()
    {
        $data = array();
        $sql = "SELECT * FROM `profilepic`";
        $q = $this->conn->query($sql);
        while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    public function show()
    {
        $sql = "SELECT * FROM `profilepic` WHERE id=:id";
        $q = $this->conn->prepare($sql);
        $q->execute(array(
            ':id' => $this->id
        ));
        $data = $q->fetch(PDO::FETCH_ASSOC);
        return $data;

    }

    public function update()
    {

        if (!empty($this->image_name)) {
            $stmt = "UPDATE `atomicprojectp4`.`profilepic` SET `profile_name` = :profile_name, `image` =:image WHERE `profilepic`.`id` =:id";
            $q = $this->conn->prepare($stmt);
            $q->execute(array(
                ':profile_name' => $this->profile_name,
                ':image' => $this->image_name,
                ':id' => $this->id,
            ));
        } else {
            $stmt = "UPDATE `atomicprojectp4`.`profilepic` SET `profile_name` = :profile_name WHERE `profilepic`.`id` =:id";
            $q = $this->conn->prepare($stmt);
            $q->execute(array(
                ':profile_name' => $this->profile_name,
                ':id' => $this->id,
            ));
        }
        if ($q) {
            session_start();
            $_SESSION['message'] = " Successfully Updated";
            header('location:index.php');
        } else {
            $_SESSION['message'] = " Not Updated Updated";
            header('location:edit.php');
        }

    }

}