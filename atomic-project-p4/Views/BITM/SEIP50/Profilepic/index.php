<?php
include_once "../../../../vendor/autoload.php";
use App\BITM\SEIP50\Profilepic\Profilepic;
use App\BITM\SEIP50\Utility\Utility;

$hobbies = new Profilepic();
$data = $hobbies->index();
//print_r($data);
$dbg = new Utility();
//$dbg->debug($data);
//die();
?>
<a href="create.php">Add New</a>
<a href="trashted.php">See deleted list</a>
<table border="1">
    <tr>
        <td>SL</td>
        <td>Name</td>
        <td>Picture</td>
        <td>Action</td>
    </tr>
    <?php
    $slno = 0;
    foreach ($data as $item) {
        $slno++;
        ?>
        <tr>
            <td>
                <?php echo $slno; ?>
            </td>
            <td>
                <?php echo $item['profile_name'] ?>
            </td>
            <td>
               <img src="<?php echo "../../../../img/".$item['image'] ?>" width="120" height="80">
            </td>
            <td>
                <a href="show.php?id=<?php echo $item['id'] ?>">Show Details</a> |
                <a href="edit.php?id=<?php echo $item['id'] ?>">Edit</a> |
                <a href="trash.php?id=<?php echo $item['id'] ?>">Delete</a>

            </td>
        </tr>
    <?php } ?>
</table>
