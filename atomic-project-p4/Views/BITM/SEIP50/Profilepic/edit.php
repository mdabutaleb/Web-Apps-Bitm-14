<?php
include_once"../../../../vendor/autoload.php";
use App\BITM\SEIP50\Profilepic\Profilepic;
use App\BITM\SEIP50\Utility\Utility;

$hob = new Profilepic();
//echo $_GET['id'];
$data = $hob->show($_GET['id']);
$dbg = new Utility();
$dbg->debug($data);

?>


<html>
<head>
    <title>
        Create | Form
    </title>
<body>
<fieldset>
    <legend>
        Update Profile Info | <a href="index.php">Back to List</a>
    </legend>
    <form action="update.php" method="post" enctype="multipart/form-data">
        <div>
            <label>Name:</label>
            <input type="text" name="name" value="<?php echo $data['profile_name'] ?>">
            <input type="file" name="photo" value="<?php echo $data['image'] ?>">
            <img src="<?php echo "../../../../img/".$data['image'] ?>" width="120" height="100">
        </div>
        <br/>

        <div>
            <input type="submit" value="Upload">
            <input type="reset" value="Reset">
        </div>
        <input type="hidden" name="id" value="<?php echo $data['id'] ?>">

    </form>
</fieldset>
</body>
</head>
</html>