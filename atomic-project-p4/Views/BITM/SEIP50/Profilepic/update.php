<?php
include_once"../../../../vendor/autoload.php";
use App\BITM\SEIP50\Profilepic\Profilepic;
use App\BITM\SEIP50\Utility\Utility;

$obj = new Profilepic();
$dbg = new Utility();

//$dbg->debug($_POST);
//////die();
//$dbg->debug($_FILES);
//die();

$oldimg = $obj->show($_POST['id']);
//$dbg->debug($oldimg);
//die();
if(isset($_FILES['photo'])){
    $errors=array();
    $file_name = time().$_FILES['photo']['name'];
    $file_type= $_FILES['photo']['type'];
    $file_tmp_name= $_FILES['photo']['tmp_name'];
    $file_size= $_FILES['photo']['size'];
$file_extension = strtolower(end(explode('.', $_FILES['photo']['name'])));
    $format = array('jpeg', 'jpg', 'png');

    if(in_array($file_extension, $format)===false){
        $errors[]= 'Wrong Extension';
    }
    if(empty($errors)==true){
        move_uploaded_file($file_tmp_name, "../../../../img/".$file_name);
        $_POST['photo']=$file_name;
    }

}
//echo "../../../../img/".$oldimg['image'];
unlink("../../../../img/".$oldimg['image']);
$obj->prepare($_POST)->update();