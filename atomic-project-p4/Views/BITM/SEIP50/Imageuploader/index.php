<?php
include_once "../../../../vendor/autoload.php";

use App\BITM\SEIP50\Imageuploader\Imageuploader;
use App\BITM\SEIP50\Utility\Utility;

$obj = new Imageuploader();

$allImage = $obj->index();

//Utility::debug($allImage);
session_start();
if (array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])) {
    echo "<h2>".$_SESSION['message']."</h2>";
    unset($_SESSION['message']);
}
?>
<a href="create.php">Add New</a> |

<table border="1">
    <tr>

        <th>SL</th>
        <th>ID</th>
        <th>Name</th>
        <th>Picture</th>
        <th>Action</th>
    </tr>
    <?php
    $sl = 1;
    foreach ($allImage as $item) { ?>
        <tr>
            <td>
                <?php echo $sl; ?>
            </td>
            <td>
                <?php echo $item['id']; ?>
            </td>
            <td>
                <?php echo $item['profile_name']; ?>
            </td>
            <td>
                <img src="<?php echo "../../../../img/" . $item['image'] ?>" width="200" height="150">
            </td>
            <td>
                <a href="view.php?id=<?php echo $item['id'] ?>">View </a>|
                <a href="edit.php?id=<?php echo $item['id'] ?>">Edit</a> |
                <a href="delete.php?id=<?php echo $item['id'] ?>">Delete</a>
            </td>

        </tr>
        <?php $sl++;
    } ?>
</table>
