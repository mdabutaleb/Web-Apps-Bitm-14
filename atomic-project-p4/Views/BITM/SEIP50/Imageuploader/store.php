<?php
include_once "../../../../vendor/autoload.php";

use App\BITM\SEIP50\Imageuploader\Imageuploader;
use App\BITM\SEIP50\Utility\Utility;

$obj = new Imageuploader();
//Utility::debug($_POST);
//Utility::debug($_FILES);

if (isset($_FILES['image'])) {
    $errors = array();
    $file_name = time() . $_FILES['image']['name'];
    $file_type = $_FILES['image']['type'];
    $file_tmp_name = $_FILES['image']['tmp_name'];
    $file_size = $_FILES['image']['size'];


    $file_extension = strtolower(end(explode('.', $_FILES['image']['name'])));
    $format = array('jpeg', 'jpg', 'png');

    if (in_array($file_extension, $format) === false) {
        $errors[] = 'Wrong Extension';
    }
    if (empty($errors) == true) {
        move_uploaded_file($file_tmp_name, "../../../../img/" . $file_name);
        $_POST['img'] = $file_name;
    }

}
//echo "../../../../img/$file_name";
//
////unlink("../../../../img/$file_name");
//die();
$obj->prepare($_POST)->store();