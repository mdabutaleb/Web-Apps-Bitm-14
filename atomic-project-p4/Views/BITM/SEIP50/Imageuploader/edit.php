<?php
include_once "../../../../vendor/autoload.php";

use App\BITM\SEIP50\Imageuploader\Imageuploader;
use App\BITM\SEIP50\Utility\Utility;
$obj = new Imageuploader();
$oneImage = $obj->prepare($_GET)->show();
Utility::debug($oneImage);
//Utility::dd($oneImage);
?>
<form action="update.php" method="post" enctype="multipart/form-data">
    <label>
        Enter Your name
    </label>
    <input type="text" name="name" value="<?php echo $oneImage['profile_name'] ?>">

    <input type="file" name="image" value="<?php echo $oneImage['image'] ?>">
    <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">
    <input type="submit" value="Update">
    <img src="<?php echo "../../../../img/".$oneImage['image']?>" width="200" height="150">
</form>