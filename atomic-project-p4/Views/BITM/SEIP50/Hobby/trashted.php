<?php
include_once"../../../../vendor/autoload.php";
use App\BITM\SEIP50\Hobby\Hobby;
use App\BITM\SEIP50\Utility\Utility;

$hobbies = new Hobby();
$data = $hobbies->trashed();
//print_r($data);
$dbg = new Utility();
//$dbg->debug($data);
?>
<a href="create.php">Add New</a> |
<a href="index.php">View Original List</a> |
<a href="trashted.php">See deleted list</a>
<table border="1">
    <tr>
        <td>SL</td>
        <td>Hobbies</td>
        <td>Action</td>
    </tr>
    <?php
    $slno = 0;
    foreach($data as $item){
        $slno++;
        ?>
        <tr>
            <td>
                <!--            --><?php //echo $item['id'] ?>
                <?php echo $slno; ?>
            </td><td>
                <?php echo $item['hobby'] ?>
            </td>
            <td>
                <a href="show.php?id=<?php echo $item['id'] ?>">Show Details</a> |
                <a href="edit.php?id=<?php echo $item['id'] ?>">Edit</a> |
                <a href="delete.php?id=<?php echo $item['id'] ?>">Delete Permanently</a>

            </td>
        </tr>
    <?php }?>
</table>
