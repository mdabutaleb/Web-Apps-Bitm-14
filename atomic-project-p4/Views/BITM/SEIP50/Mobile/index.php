<?php
//include_once"../../../../src/BITM/SEIP50/Mobile/Mobile.php";
include_once "../../../../vendor/autoload.php";
use App\BITM\SEIP50\Mobile\Mobile;
use App\BITM\SEIP50\Utility\Utility;

$mobiles = new Mobile();
$data = $mobiles->index();
//print_r($data);
$dbg = new Utility();
//$dbg->debug($data);
?>
<a href="create.php">Add New</a> |
<a href="pdf.php" target="_blank">Save As PDF</a> |
<a href="xlx.php" target="_blank">Save As Excel</a>
<table border="1">
    <tr>
        <td>SL</td>
        <td>Mobile Model</td>
        <td>Action</td>
    </tr>
    <?php
    $slno = 0;
    foreach ($data as $item) {
        $slno++;
        ?>
        <tr>
            <td>

                <?php echo $slno; ?>
            </td>
            <td>
                <?php echo $item['title'] ?>
            </td>
            <td>
                <a href="show.php?id=<?php echo $item['id'] ?>">Show Details</a> |
                <a href="edit.php?id=<?php echo $item['id'] ?>">Edit</a> |
                <a href="delete.php?id=<?php echo $item['id'] ?>">Delete</a>

            </td>
        </tr>
    <?php } ?>
</table>
