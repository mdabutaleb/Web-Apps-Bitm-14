<?php
include_once"../../../../vendor/autoload.php";
use App\BITM\SEIP50\Mobile\Mobile;
use App\BITM\SEIP50\Utility\Utility;

$mobiles = new Mobile();
$data = $mobiles->prepare($_POST)->update();
$dbg = new Utility();
$dbg->debug($_POST);